{
 "case_id"=>"jamesbondagent47",
 "first_name"=>"James",
 "middle_name"=>"-",
 "last_name"=>"Bond",
 "gender"=>"male",
 "dob"=>"01/19/1993",
 "alias"=>"Jimmy",
 "details"=>{"ethnicity"=>["ethincity"],
             "emails"=>["jamesbond@gmail.com"],
             "phones"=>[{"phone_number"=>"1122334455", "type"=>"Home"},
                         {"phone_number"=>"15123435468", "type"=>"Cell"}],
             "addresses"=>[{"type"=>"Current", "address"=>"Street # 10", "city"=>"Lahore", "state"=>"Punjab", "zip"=>"54000"}],
             "employment"=>["Programming"],
             "schools"=>["Cantt Public School"],
             "relationships"=>["brother"],
             "physical_descriptions"=>["slim bean", "muscular"],
             "report_highlights"=>"He is Agent 47",
             "items_added"=>""
           },
 "claim"=>"ttd",
 "from"=>"08/01/2017",
 "orders_attributes"=>{"0"=>{"kind"=>"analyst_quick_check"}},
 "description"=>"testing api for james bond",
 "files_attributes"=>{
   "0"=>{"kind"=>"user_upload",
     "attachment_attributes"=>{
       "file"=>"#<ActionDispatch::Http::UploadedFile:0x0000000d1a9f98 @tempfile=#<Tempfile:/tmp/RackMultipart20180105-21306-1c3y5bb.jpg>,
       @original_filename=\"detective.jpg\", @content_type=\"image/jpeg\", @headers=\"Content-Disposition: form-data;
       name=\"person[files_attributes][0][attachment_attributes][file]\"; filename=\"detective.jpg\"\r\nContent-Type: image/jpeg\r\n\">"
     }
   }
 }
}
