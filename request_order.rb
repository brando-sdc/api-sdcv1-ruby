def self.request_order(params)
    auth = {
      :username => "#{your_username}"
      :password => "#{your_password}"
    }

    headers = {
      "apikey" => "#{apikey}",
      "secret" => "#{secretKey}"
    }

    response = HTTParty.post("#{http - api endpoint path}",
                  :basic_auth => auth, :headers => headers, :body =>
            {
             "person":
              {"case_id": "#{params['order']['case_id']}",
               "first_name": "#{params['order']['first_name']}",
               "middle_name": "#{params['order']['middle_name']}",
               "last_name": "#{params['order']['last_name']}",
               "gender": "male",
               "dob": "01/19/1993",
               "alias": "apio",
               "details":
                {"ethnicity": ["ethnicity"],
                 "emails": ["test123@gmail.com"],
                 "phones":
                  [{"phone_number": "12312312321", "type": "Cell"},
                   {"phone_number": "1235324234", "type": "Home"}],
                 "addresses":
                  [{"type": "Current",
                    "address": "street 41",
                    "city": "jjjj",
                    "state": "sss",
                    "zip": "54000"}],
                 "employment": ["developer"],
                 "schools": ["becon house"],
                 "relationships": ["brother"],
                 "physical_descriptions": ["slim bean", "muscular"],
                 "report_highlights": " testing",
                 "items_added": ""},
               "claim": "ttd",
               "from": "08/01/2017",
               "orders_attributes":
                {"0":
                  {"networks": "",
                   "specifications": {"advance_options": ["Facebook - Friends"]},
                   "kind": "analyst_quick_check"
                  }
                },
            	   "description": "testing apio staging"
               },
             "to": "Today"
            })
    puts response
  end
